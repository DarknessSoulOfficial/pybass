from setuptools import setup, find_packages

setup(
    name="PythonBass",
    description="Python Binding For BASS Library",
    version=str('1.0'),
    author="DarknessSoul",
    license="MIT",
    packages=find_packages(["PyBass"]),
)
