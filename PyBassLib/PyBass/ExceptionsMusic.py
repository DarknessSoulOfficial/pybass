class MusicIsNotInitializatedException(Exception):
    def __init__(message="MP3 Music Is Not Founded"):
        self.message = message
        super().__init__(message)
        pass
