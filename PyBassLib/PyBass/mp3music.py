from ExceptionsMusic import MusicIsNotInitializatedException
import os
import ctypes

class MP3Music():
    @staticmethod
    def FindLibraryPyBass():
        pythonpybass = ctypes.cdll.LoadLibrary("{}".format(os.getcwd() + "\\PyBass_Cpp.dll"))
        if pythonpybass is None:
            raise MusicIsNotInitializatedException()
        else:
            print("PyBass Binary Is Founded!!!")
        return pythonpybass
    def InitMP3(filename: str):
        pybassbin = MP3Music.FindLibraryPyBass()
        pybassbin.PlayMusicInBass(filename)
